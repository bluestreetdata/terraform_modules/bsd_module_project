


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| billing\_id | billing id | `string` | n/a | yes |
| bsd\_labels | labels to be applied | `map` | <pre>{<br>  "org": "bsd"<br>}</pre> | no |
| folder\_id | The folder the project will be created in | `string` | n/a | yes |
| org\_id | gcp organisation id | `string` | n/a | yes |
| vpc\_host | the name of the shared vpc host project | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | The ID of the project in which resources are provisioned. |
| project\_name | The name of the bucket. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->